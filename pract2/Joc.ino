// set pin numbers:
const int buttonPin = PUSH1;     // the number of the pushbutton pin

const int ldrPin =  78;      // the number of the LDR input pin

//User defined constants:
const int V_max = 0;
const int V_min = 0;
const int t_no_visible = 2000;

// variables will change:
int buttonState = 1;         // variable for reading the pushbutton status
int num = 0;
bool hidden = 0;
unsigned int startTime;

void setup() {
    // initialize the LDR pin as an input:
    pinMode(ldrPin, INPUT);      
    // initialize the pushbutton pin as an input:
    pinMode(buttonPin, INPUT_PULLUP);     
    Serial.begin(115200);

}

void loop(){
    delay(100);

    // read the state of the pushbutton value:
    buttonState = digitalRead(buttonPin);
    unsigned int currentMillis = millis();
    if(hidden) //If the data is hidden
    {
        if(currentMillis - startTime > t_no_visible) //If time is over
        {
            hidden = 0; //Change state
            num = analogRead(ldrPin); //Print real data
        }
        else
        {
            num = 0; //Hide data
        }
    }
    else
    {
        num = analogRead(ldrPin); //Print real data
        if(buttonState == HIGH) //If button is pressed
        {
            hidden = 1; //Set state to hidden 
            startTime = millis(); //Start counting time
        }
    }

    //Print values
    Serial.print(V_min);
    Serial.print(",");
    Serial.print(V_max);
    Serial.print(",");
    Serial.print(num)

}
