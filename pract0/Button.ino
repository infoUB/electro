/*
  Button
 
 Turns on and off a light emitting diode(LED) connected to digital  
 pin 13, when pressing a pushbutton attached to pin 2. 
 
 
 The circuit:
 * LED attached from pin 13 to ground 
 * pushbutton attached to pin 2 from +3.3V
 * 10K resistor attached to pin 2 from ground
 
 * Note: on most Arduinos there is already an LED on the board
 attached to pin 13.
 
 
 created 2005
 by DojoDave <http://www.0j0.org>
 modified 30 Aug 2011
 by Tom Igoe
 modified Apr 27 2012
 by Robert Wessels
 
 This example code is in the public domain.
 
 http://www.arduino.cc/en/Tutorial/Button
 */

 //Funcions
void toggleLeds(int state);

// constants won't change. They're used here to 
// set pin numbers:
const int buttonPin = PUSH1;     // the number of the pushbutton pin
const int buttonPin2 = PUSH2; //Segon boto
const int ledPinR =  75;      // the number of the LED pin
const int ledPinG =  76;      // the number of the LED pin
const int ledPinB =  77;      // the number of the LED pin


// variables will change:
int buttonState = 0;         // variable for reading the pushbutton1 status
int buttonState2 = 0;       // variable for reading the pushbutton2 status

void setup() {
  // initialize the LEDs pins as an output:
  pinMode(ledPinR, OUTPUT);      
  pinMode(ledPinG, OUTPUT);      
  pinMode(ledPinB, OUTPUT);      

  // initialize the pushbuttons pins as an input:
  pinMode(buttonPin, INPUT_PULLUP);     
  pinMode(buttonPin2, INPUT_PULLUP);    
   
  Serial.begin(115200);

}


/*
Encen els LEDs RGB quan els dos botons estan polsats. Aixo es comprova cada 100ms.
*/
void loop(){
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);
  buttonState2 = digitalRead(buttonPin2);

  //Imprimim estats dels botons en consola
  Serial.print(buttonState);
  Serial.print(", ");
  Serial.print(buttonState2);
  Serial.println();

  //Comprovem si els dos botons estan polsats
  if (buttonState == LOW && buttonState2 == LOW) 
  {     
    // turn LED on:    
    toggleLeds(HIGH);

  } 
  
  else 
  {
    // turn LED off:
    toggleLeds(LOW);
  }

  //Afegim un delay de 100 segons
  delay(100);
}

//Encen o apaga les llums RGB
void toggleLeds(int state)
{
    digitalWrite(ledPinR, state);  
    digitalWrite(ledPinG, state);  
    digitalWrite(ledPinB, state); 
}


