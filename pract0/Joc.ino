/*
  Button
 
 Turns on and off a light emitting diode(LED) connected to digital  
 pin 13, when pressing a pushbutton attached to pin 2. 
 
 
 The circuit:
 * LED attached from pin 13 to ground 
 * pushbutton attached to pin 2 from +3.3V
 * 10K resistor attached to pin 2 from ground
 
 * Note: on most Arduinos there is already an LED on the board
 attached to pin 13.
 
 
 created 2005
 by DojoDave <http://www.0j0.org>
 modified 30 Aug 2011
 by Tom Igoe
 modified Apr 27 2012
 by Robert Wessels
 
 This example code is in the public domain.
 
 http://www.arduino.cc/en/Tutorial/Button
 */

// constants won't change. They're used here to 
// set pin numbers:
const int buttonPin = PUSH1;     // the number of the pushbutton pin
const int buttonPin2 = PUSH2;     // the number of the pushbutton pin

const int ledPin =  78;      // the number of the LED pin

// variables will change:
int buttonState = 1;         // variable for reading the pushbutton status
int buttonState2 = 1;
int num = 0;
int condition = 0;

void setup() {
  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);      
  // initialize the pushbutton pin as an input:
  pinMode(buttonPin, INPUT_PULLUP);     
  pinMode(buttonPin2, INPUT_PULLUP);
  Serial.begin(115200);

}

void loop(){
  delay(100);
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);
  buttonState2 = digitalRead(buttonPin2);

  Serial.print(buttonState);
  Serial.print(",");
  Serial.print(buttonState2);
  Serial.print(",");
  Serial.println(condition);
  if(condition == 0 && num > 990)
  {
    condition = 1;
  }

  if(condition == 1)
  {
    digitalWrite(ledPin, HIGH);
    if(buttonState == LOW || buttonState2 == LOW)
    {
      condition = 0;
    }
  }
  else
  {
    num = random(1000);
    digitalWrite(ledPin, LOW);
  }
  
  // check if the pushbutton is pressed.
  // if it is, the buttonState is HIGH:
  if (buttonState == HIGH) {     
    // turn LED on:    
    digitalWrite(ledPin, HIGH);  
  } 
  else {
    // turn LED off:
    digitalWrite(ledPin, LOW); 
  }
}
