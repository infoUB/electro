// set pin numbers:

const int fdPin =  40;      // the number of the photodiode input pin
const int buzzerPin =  31;

//User defined constants:
const int limitTime = 500;

// variables will change:
bool lightState = 0;        
bool light = 0;
int num;
unsigned int startTime;
unsigned int currentMillis;

void setup() 
{
    // initialize the LDR pin as an input:
    pinMode(fdPin, INPUT);      
    pinMode(buzzerPin, OUTPUT);
    Serial.begin(115200);

}

void loop()
{
    delay(10);
    
    lightState = digitalRead(fdPin);
    /*lightState = analogRead(fdPin);*/
        
    if(light) //If we were recieving light
    {
        
        if(!lightState) //If we stop recieving light
        {
            currentMillis = millis(); //Stop counter
            light = 0; //Change state
            
            if(currentMillis - startTime > limitTime) //If 1
            {
                Serial.print("1");
                tone(buzzerPin, 493, 201);
            }
            else
            {
                Serial.print("0");
                tone(buzzerPin, 261, 201);
            }       
        }
    }
    else //If we were not recieving light
    {
        if(lightState) //If we recieve light
        {
            startTime = millis(); //Start counter
            light = 1; //Change state
        }
    }
}
        
