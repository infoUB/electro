#include "notes_frequencies.h"

//Pins entrada i sortida
const int outPin = 31;
const int inPin = 40;
//Notes i duracio d'elles
unsigned int notes[] = {ND3,ND3,ND3,NG3,ND4,NC4,NB3,NA3,NG4,ND4,NC4,NB3,NA3,NG4,ND4,NC4,NB3,NC4,NA3};
unsigned int duracio_notes[] = {2,2,2,8,8,2,2,2,8,4,2,2,2,8,4,2,2,2,4};
//Temps escala
unsigned int t0=100;
//Contador pel index del vector
unsigned int cont = 0;

void setup() 
{
    pinMode(inPin, INPUT);      
    pinMode(outPin, OUTPUT);
    Serial.begin(115200);

}

void loop() 
{
  //Quan s'apreti el boto
    if(digitalRead(inPin))
    {
      //Reproduir el so
      tone(outPin,notes[cont],duracio_notes[cont]*t0);
      //Esperar a acabar la reproduccio
      delay(duracio_notes[cont]*t0);
      //Anar a la següent nota
      cont = (cont + 1)%(sizeof(notes)/sizeof(int));


      //Manual debugs for lab
      //Serial.println((sizeof(notes)/sizeof(int));
      //Serial.println(digitalRead(inPin));
    }
}
